//
//  CameraPresenter.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 15.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import AVFoundation
import UIKit
import AVKit
import GLKit

//protocol CameraPresenterProtocol {
//    func setViewAsPreview(view: UIView)
//    func captureDeviceInput()
//    init(cameraVCProtocol: CameraViewControllerProtocol) {
//    <#statements#>
//    }
//}
//
//class CameraPresenter: AVCaptureVideoDataOutputBufferSampleBufferDelegate {
//    
//    
//    
//    var devicePosition: AVCaptureDevicePosition? {
//        didSet {
//            switchDevicePosition()
//        }
//    }
//    
//    let cameraSession = AVCaptureSession()
//    let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo) as AVCaptureDevice
//    
//    func createSession(createSessionCompletion completion:() -> (Void)) {
//        cameraSession.sessionPreset = AVCaptureSessionPresetHigh
//    }
//    
//    func captureDeviceInput() {
//        createSession { () -> (Void) in
//            do {
//                let deviceInput = try AVCaptureDeviceInput(device: captureDevice)
//                cameraSession.beginConfiguration()
//                
//                if (cameraSession.canAddInput(deviceInput) == true) {
//                    cameraSession.addInput(deviceInput)
//                } else {
//                    NSLog("Failed to add input [36]")
//                }
//                
//                let dataOutput = AVCaptureVideoDataOutput()
//                dataOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString) : NSNumber(value: kCVPixelFormatType_420YpCbCr8BiPlanarFullRange as UInt32)]
//                dataOutput.alwaysDiscardsLateVideoFrames = true
//                
//                let queue = DispatchQueue(label: "com.invasivecode.videoQueue")
//                dataOutput.setSampleBufferDelegate(self, queue: queue)
//            } catch let error as Error {
//                NSLog("\(error), \(error.localizedDescription)")
//            }
//        }
//    }
//    
//    func switchDevicePosition() {
//        
//    }
//    
//    func setViewAsPreview(view: UIView) {
//    
//    }
//    
//    lazy var previewLayer: AVCaptureVideoPreviewLayer = {
//        
//        let preview = AVCaptureVideoPreviewLayer(session: self.cameraSession)
//        preview?.bounds = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
//        preview?.position = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
//        preview?.videoGravity = AVLayerVideoGravityResize
//        return preview
//    }()
//    
//    //MARK: AVCaptureVideoDataOutputBufferSampleBufferDelegate Protocol
//    
//    func captureOutput(_ captureOutput: AVCaptureOutput!, didOuputSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!) {
//    
//    }
//}
