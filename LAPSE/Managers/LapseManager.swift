//
//  LapseManager.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 21.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit
import RealmSwift

struct LapseManager {

    static var shared = LapseManager()
    
    var currentLapse : Lapse?
    
    mutating func addPhoto(photo: UIImage, description:String?, date: Date) {
        let lapseItem = LapseItem()
        lapseItem.image = UIImageJPEGRepresentation(photo, 1)
        lapseItem.photoDescription = description
        lapseItem.date = Date()
        
        if let currentLapse = currentLapse {
            let realm = try! Realm()
            try! realm.write {
                currentLapse.items.append(lapseItem)
            }
        } else {
            self.createLapse(with: lapseItem, for: UserManager.shared.getUser())
        }
    }
    
    mutating func createLapse(with lapseItem: LapseItem, for user: User?) {
        self.currentLapse = Lapse()
        currentLapse?.items.append(lapseItem)
        let realm = try! Realm()
        if let user = user {
            try! realm.write {
                user.lapses.append(currentLapse!)
            }
        } else {
            let unkownUser = realm.object(ofType: User.self, forPrimaryKey: "-1")!
            try! realm.write {
                unkownUser.lapses.append(currentLapse!)
            }
        }
    }
    
    func deleteLapse(lapse: Lapse?, lapseDeleteCompletion completion:() -> (Void)) {
        guard let lapse = lapse, let realm = lapse.realm else { return }
        try! realm.write {
            realm.delete(lapse)
            completion()
        }
    }
    
    func deletePhoto(atIndex index: Int?, from lapse: Lapse? = nil) {
        guard let lapse = lapse ?? self.currentLapse, let index = index else { return }
        let realm = try! Realm()
        let photo = lapse.items[index]
        try! realm.write {
            lapse.items.remove(objectAtIndex: index)
            realm.delete(photo)
        }
    }
}








