//
//  ParsingError.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 10.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit

enum ParsingError: Error {
    case badJSON
}
