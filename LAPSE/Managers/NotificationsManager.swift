//
//  NotificationsManager.swift
//  LAPSE
//
//  Created by valivaxa on 4/2/17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class NotificationsManager {
    
    static let shared = NotificationsManager()
    
    
    private init() {
        if everydayNotificationsAvailable == nil {
            everydayNotificationsAvailable = true
        }
        if yearNotificationsAvailable == nil {
            yearNotificationsAvailable = false
        }
    }
    
    var everydayNotificationsAvailable: Bool! {
        get {
            return UserDefaults.standard.value(forKey: "EVERYDAY_ENABLED") as? Bool
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "EVERYDAY_ENABLED")
            UserDefaults.standard.synchronize()
            if !newValue {
                unregisterLocalNotifications()
            }
            else {
                registerLocalNotifications()
            }
        }
    }
    var yearNotificationsAvailable: Bool! {
        get {
            return UserDefaults.standard.value(forKey: "YEAR_ENABLED") as? Bool
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "YEAR_ENABLED")
            UserDefaults.standard.synchronize()
        }
    }
    
    func registerLocalNotifications() {
        UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types:[.alert, .badge, .sound], categories: nil))
        let notification = UILocalNotification()
        notification.alertBody = "Its time to make a new lapse!"
        notification.fireDate = Calendar.current.date(bySetting: .hour, value: 12, of: Date())
        notification.repeatInterval = .day
        notification.category = "EVERYDAY_NOTIFICATION"
        notification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.shared.scheduleLocalNotification(notification)
    }
    
    private func unregisterLocalNotifications() {
        UIApplication.shared.cancelAllLocalNotifications()
    }
}
