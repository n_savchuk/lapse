//
//  UserManager.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 09.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit
import Foundation
import FacebookLogin
import FacebookCore
import SwiftyJSON
import SDWebImage
import RealmSwift

struct UserProfileRequest: GraphRequestProtocol {
    struct Response: GraphResponseProtocol {
        var id: String
        var name: String
        var email: String
        var photo: String
        init(rawResponse: Any?) {
            let json = JSON(rawResponse as Any)
            id = json["id"].stringValue
            name = json["name"].stringValue
            email = json["email"].stringValue
            photo = json["picture"].stringValue
        }
    }
    
    var graphPath = "/me"
    var parameters: [String : Any]? = ["fields" : "id, name, email"]
    var accessToken = AccessToken.current
    var httpMethod: GraphRequestHTTPMethod = .GET
    var apiVersion: GraphAPIVersion = .defaultVersion
}

class UserManager: NSObject {
    
    static let shared = UserManager()
    
    private let loginManager = LoginManager()
    
    fileprivate var currentUser: User?
    
    func setUser(with accessToken: AccessToken, completion: @escaping () -> (Void)) {
        let request = UserProfileRequest()
        let connection = GraphRequestConnection()
        connection.add(request) { response, result in
            switch result {
            case .success(let response):
                NSLog("Custom Graph Request Succeeded: \(response)")
                NSLog("User facebook id is \(response.id)")
                NSLog("User name is \(response.name)")
                NSLog("User email is \(response.email)")
                self.getProfileImage(userID: response.id, completion: { (image, error) in
                    if error == nil {
                        let user = User()
                        user.email = response.email
                        user.fullName = response.name
                        user.id = response.id
                        user.photo = UIImageJPEGRepresentation(image!, 1)
                        user.accessToken = accessToken.authenticationToken
                        user.isActive = true
                        self.saveUser(user: user)
                    } else {
                        NSLog("\(String(describing: error?.localizedDescription))")
                    }
                })
            case .failed(let error):
                NSLog("Custom Graph Request Failed: \(error.localizedDescription)")
            }
        }
        connection.start()
    }
    
    func getUser() -> User? {
        let realm = try! Realm()
        return realm.objects(User.self).filter("isActive = true").first
    }
    
    private func saveUser(user: User?) {
        if let user = user {
            let realm = try! Realm()
            let lapses = realm.object(ofType: User.self, forPrimaryKey: user.id)?.lapses
            if let lapses = lapses {
                user.lapses.append(objectsIn: lapses)
            }
            try! realm.write {
                realm.add(user, update: true)
                NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "userDidLogin")))
            }
        }
    }
    
    func login(from viewController: UIViewController, completion: @escaping (Void) -> ()) {
        loginManager.loginBehavior = .native
        loginManager.logIn([ .publicProfile, .email], viewController: viewController) { (LoginResult) in
            switch LoginResult {
            case .cancelled:
                NSLog("User cancelled login")
            case .failed(let error):
                NSLog("Login error: \(error.localizedDescription)")
            case .success(grantedPermissions: let grantedPermissions, declinedPermissions: let declinedPermissions, token: let accessToken):
                NSLog("Granted permissions :\n-\(grantedPermissions)")
                NSLog("Declined permissions: \(declinedPermissions)")
                NSLog("Access token: \(accessToken)")
                self.setUser(with: accessToken, completion: {
                    completion()
                })
            }
        }
    }
    
    func logout() {
        if let user = getUser() {
            loginManager.logOut()
            let realm = try! Realm()
            try! realm.write {
                user.isActive = false
                NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "userDidLogin")))
            }
        } else {
            NSLog("No active user yet")
        }
    }
    
    func userIsLoggined() -> Bool {
        let user = getUser()
        if user != nil {
            return true
        } else {
            NSLog("User not found")
            return false
        }
    }
    
    private func getProfileImage(userID: String, completion: @escaping (_ image: UIImage?, _ error: Error?) -> ()) {
        guard let profileImageURL = URL(string: "https://graph.facebook.com/\(userID)/picture?type=large") else {
            NSLog("Wrong URL")
            return
        }
        SDWebImageManager.shared().downloadImage(with: profileImageURL, options: .avoidAutoSetImage, progress: nil, completed: { (image, error, cachedType, bool, url) in
            if error == nil {
                completion(image!, nil)
            } else {
                completion(nil, error)
                NSLog("\(String(describing: error?.localizedDescription))")
            }
        })
    }
}
