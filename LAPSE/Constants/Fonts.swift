//
//  Fonts.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 10.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit

class Fonts {
    
    static let shared = Fonts()
    
    
    let proximaNovaRegular = "ProximaNova-Regular"
    let proximaNovaLight = "ProximaNova-Light"
    let gothamProMedium = "GothamPro-Medium"
    
    static let lapseItemDescriptionFont = UIFont(name: Fonts.shared.proximaNovaRegular, size: 20)
    
    //28
    static func userName(string: String) -> NSAttributedString {
        let attributes: [String : AnyObject] = [
            NSFontAttributeName : UIFont(name: Fonts.shared.proximaNovaRegular, size: 28)!,
            NSForegroundColorAttributeName : Colors.black,
            NSKernAttributeName : 0.3 as AnyObject]
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
    
    //18
    static func lapseNumer(string: String) -> NSAttributedString {
        let attributes: [String : AnyObject] = [
            NSFontAttributeName : UIFont(name: Fonts.shared.proximaNovaRegular, size: 18)!,
            NSForegroundColorAttributeName : Colors.gray,
            NSKernAttributeName : 0.2 as AnyObject]
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
    
    static func menuItems(string: String) -> NSAttributedString {
        let attributes: [String : AnyObject] = [
            NSFontAttributeName : UIFont(name: Fonts.shared.proximaNovaRegular, size: 18)!,
            NSForegroundColorAttributeName : Colors.black,
            NSKernAttributeName : 0.2 as AnyObject]
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
    
    //Login & Logout
    static func facebookLogin(string: String) -> NSAttributedString {
        let attributes: [String : AnyObject] = [
            NSFontAttributeName : UIFont(name: Fonts.shared.proximaNovaRegular, size: 18)!,
            NSForegroundColorAttributeName : Colors.facebookColor,
            NSKernAttributeName : 0.2 as AnyObject]
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
    
    static func facebookLogout(string: String) -> NSAttributedString {
        let attributes: [String : AnyObject] = [
            NSFontAttributeName : UIFont(name: Fonts.shared.proximaNovaRegular, size: 18)!,
            NSForegroundColorAttributeName : Colors.red,
            NSKernAttributeName : 0.2 as AnyObject]
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
    
    //Lapse Item Description
    static func lapseItemDescriptionPlaceholder(string: String) -> NSAttributedString {
        let attributes: [String : AnyObject] = [
            NSFontAttributeName : UIFont(name: Fonts.shared.proximaNovaRegular, size: 20)!,
            NSForegroundColorAttributeName : Colors.white.withAlphaComponent(0.6),
            NSKernAttributeName : 0.2 as AnyObject]
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
    
    static func lapseItemDescription(string: String) -> NSAttributedString {
        let attributes: [String : AnyObject] = [
            NSFontAttributeName : UIFont(name: Fonts.shared.proximaNovaRegular, size: 20)!,
            NSForegroundColorAttributeName : Colors.white.withAlphaComponent(1.0),
            NSKernAttributeName : 0.2 as AnyObject]
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
    
    static func lapseItemDate(string: String) -> NSAttributedString {
        let attributes: [String : AnyObject] = [
            NSFontAttributeName : UIFont(name: Fonts.shared.proximaNovaRegular, size: 62)!,
            NSForegroundColorAttributeName : Colors.white.withAlphaComponent(1.0),
            NSKernAttributeName : 0.7 as AnyObject]
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
}
