//
//  AppDelegate.swift
//  PULSE
//
//  Created by Nikita Savchuk on 02.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import FBSDKShareKit
import Bolts
import Fabric
import Crashlytics
import SwiftyStoreKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        Fabric.with([Crashlytics.self])
        
        SwiftyStoreKit.completeTransactions(atomically: true) { products in
            for product in products {
                if product.transaction.transactionState == .purchased || product.transaction.transactionState == .restored {
                    if product.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(product.transaction)
                    }
                    print("purchased: \(product)")
                }
            }
        }
        
        self.setupGlobalUI()
//        self.showProjectFonts()
        
        let storedUser = (try! Realm().object(ofType: User.self, forPrimaryKey: "-1"))
        if storedUser == nil {
            let user = User()
            user.id = "-1"
            let realm = try! Realm()
            try! realm.write {
                realm.add(user)
            }
        }
        
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
//    fileprivate func showProjectFonts() {
//        for familyName in UIFont.familyNames {
//            for fontName in UIFont.fontNames(forFamilyName: familyName ) {
//                print("\(familyName) : \(fontName)")
//            }
//        }
//    }
    
    fileprivate func setupGlobalUI() {
        //MARK: Setup Navigation Controller
        UINavigationBar.appearance().backgroundColor = Colors.white
        UINavigationBar.appearance().barTintColor = Colors.white
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        UINavigationBar.appearance().shadowImage = UIImage()
        
        //MARK: Setup Status Bar
        UIApplication.shared.statusBarStyle = .default
        
        
        //MARK: Setup Page Control
        let pageControl = UIPageControl.appearance()
        pageControl.pageIndicatorTintColor = Colors.unselectedPageIndicator
        pageControl.currentPageIndicatorTintColor = Colors.selectedPageIndicator
        pageControl.backgroundColor = Colors.white
    }
}

