//
//  CameraViewController.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 14.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//
//  CameraViewControllerID

import UIKit
import Foundation
import AVFoundation
import NextLevel

class CameraViewController: UIViewController, NextLevelDelegate, NextLevelPhotoDelegate, NextLevelFlashAndTorchDelegate {
    
    fileprivate var capturedImage : UIImage! {
        didSet {
            if capturedImage != nil {
                self.cameraLayaoutAnimation()
            }
        }
    }
    fileprivate var cameraPreviewView: UIView?
    
    
    var currentLapse : Lapse?
    
    @IBOutlet weak var headerView: CameraHeaderView!
    @IBOutlet weak var footerView: CameraFooterView!
    @IBOutlet weak var previewView: PhotoPreviewView!
    @IBOutlet weak var maskImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.currentLapse = LapseManager.shared.currentLapse
        self.setupCameraPreview()
        self.configureCaptureSession()
        NextLevel.shared.delegate = self
        NextLevel.shared.photoDelegate = self
        NextLevel.shared.requestAuthorization(forMediaType: AVMediaTypeVideo)
        self.setupNib()
        self.setInitialUI()
        if let lapse = LapseManager.shared.currentLapse, lapse.items.count > 0 {
            maskImageView.image = UIImage(data: (currentLapse?.items.last?.image)!)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        do {
            try NextLevel.shared.start()
        } catch {
            NSLog("Error with session start: \(error)")
        }
        UIApplication.shared.isStatusBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NextLevel.shared.stop()
        UIApplication.shared.isStatusBarHidden = false
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private func setupNib() {
        headerView.setCloseButtonTapHandler {
            self.dismiss(animated: true, completion: {
                NSLog("Did close camera")
            })
        }
        headerView.setFlashButtonTapHandler {
            if NextLevel.shared.isFlashAvailable {
                if NextLevel.shared.flashMode == .off {
                    NextLevel.shared.flashMode = .on
                    self.headerView.toogleFlash.setImage(UIImage(named:"flash_on"), for: UIControlState())
                } else {
                    NextLevel.shared.flashMode = .off
                    self.headerView.toogleFlash.setImage(UIImage(named:"flash_off"), for: UIControlState())
                }
            }
            
        }
        headerView.setSwitchCameraButtonTapHandler {
            NextLevel.shared.flipCaptureDevicePosition()
        }
        
        footerView.setCapturePhotoButtonTapHandler {
            if NextLevel.shared.canCapturePhoto {
                NextLevel.shared.capturePhoto()
            }
        }
        
        previewView.closeButtonTapHandler {
            let alert = UIAlertController(title: NSLocalizedString("exit_adding_photo_alert_title", comment: ""), message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: NSLocalizedString("continue", comment: ""), style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: NSLocalizedString("undo", comment: ""), style: .destructive, handler: { _ in
                self.dismiss(animated: true, completion: {
                    NSLog("Did close camera")
                })
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        previewView.confirmButtonTapHandler {
            LapseManager.shared.addPhoto(photo: self.capturedImage!,
                                         description: self.previewView.descriptionTexField?.text,
                                         date: Date())
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "photoPreviewViewControllerID")
            self.present(vc!, animated: true, completion: {
                self.footerView.isHidden = true
                self.headerView.isHidden = true
                NextLevel.shared.stop()
            })
        }
        
        previewView.doneButtonTapHandler {
            self.previewView.descriptionTexField.resignFirstResponder()
        }
        
        previewView.retakePhotoButtonTapHandler {
            let alert = UIAlertController(title: NSLocalizedString("reatke_photo_alert_title", comment: ""), message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: NSLocalizedString("reatke_photo_alert_stay", comment: ""), style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: NSLocalizedString("reatke_photo_alert_retake", comment: ""), style: .destructive, handler: { _ in
                self.previewLayaoutAnimation()
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func setupCameraPreview() {
        let screenBounds = UIScreen.main.bounds
        self.cameraPreviewView = UIView(frame: screenBounds)
        if let cameraPreviewView = self.cameraPreviewView {
            cameraPreviewView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            NextLevel.shared.previewLayer.frame = cameraPreviewView.bounds
            cameraPreviewView.layer.addSublayer(NextLevel.shared.previewLayer)
            self.view.insertSubview(cameraPreviewView, at: 0)
        }
    }

    func configureCaptureSession() {
        NextLevel.shared.photoStabilizationEnabled = true
        NextLevel.shared.automaticallyUpdatesDeviceOrientation = true
        NextLevel.shared.videoStabilizationMode = .auto
        NextLevel.shared.captureMode = .photo
        NextLevel.shared.focusMode = .continuousAutoFocus
        NextLevel.shared.exposureMode = .continuousAutoExposure
    }
 
    func nextLevel(_ nextLevel: NextLevel, willCapturePhotoWithConfiguration photoConfiguration: NextLevelPhotoConfiguration) {
    
    }
    func nextLevel(_ nextLevel: NextLevel, didCapturePhotoWithConfiguration photoConfiguration: NextLevelPhotoConfiguration) {
        print("zalupa")
    }

    func nextLevel(_ nextLevel: NextLevel, didProcessPhotoCaptureWith photoDict: [String: Any]?, photoConfiguration: NextLevelPhotoConfiguration) {
        let imageData = photoDict?[NextLevelPhotoJPEGKey]
        if let imageData = imageData as? Data {
            capturedImage = UIImage(data: imageData)
        }
    
    }
    func nextLevel(_ nextLevel: NextLevel, didProcessRawPhotoCaptureWith photoDict: [String: Any]?, photoConfiguration: NextLevelPhotoConfiguration) {
    
    }
    
    func nextLevelDidCompletePhotoCapture(_ nextLevel: NextLevel) {
    
    }

    func nextLevel(_ nextLevel: NextLevel, didUpdateAuthorizationStatus status: NextLevelAuthorizationStatus, forMediaType mediaType: String) {
        do {
            try NextLevel.shared.start()
        } catch {
            NSLog("Error with session start: \(error)")
        }
    }

    // configuration
    func nextLevel(_ nextLevel: NextLevel, didUpdateVideoConfiguration videoConfiguration: NextLevelVideoConfiguration) {}
    func nextLevel(_ nextLevel: NextLevel, didUpdateAudioConfiguration audioConfiguration: NextLevelAudioConfiguration) {}
    
    // session
    func nextLevelSessionWillStart(_ nextLevel: NextLevel) {}
    func nextLevelSessionDidStart(_ nextLevel: NextLevel) {}
    func nextLevelSessionDidStop(_ nextLevel: NextLevel) {}
    
    // session interruption
    func nextLevelSessionWasInterrupted(_ nextLevel: NextLevel) {}
    func nextLevelSessionInterruptionEnded(_ nextLevel: NextLevel) {}
    
    // preview
    func nextLevelWillStartPreview(_ nextLevel: NextLevel) {}
    func nextLevelDidStopPreview(_ nextLevel: NextLevel) {}
    
    // mode
    func nextLevelCaptureModeWillChange(_ nextLevel: NextLevel) {}
    func nextLevelCaptureModeDidChange(_ nextLevel: NextLevel) {}
    
    // position, orientation
    func nextLevelDevicePositionWillChange(_ nextLevel: NextLevel) {}
    func nextLevelDevicePositionDidChange(_ nextLevel: NextLevel) {}
    func nextLevelDidChangeFlashMode(_ nextLevel: NextLevel) {}
    func nextLevelDidChangeTorchMode(_ nextLevel: NextLevel) {}
    func nextLevelFlashActiveChanged(_ nextLevel: NextLevel) {}
    func nextLevelTorchActiveChanged(_ nextLevel: NextLevel) {}
    func nextLevelFlashAndTorchAvailabilityChanged(_ nextLevel: NextLevel) {}
    
    
    func cameraLayaoutAnimation() {
        UIView.animate(withDuration: 1, animations: { 
            self.headerView.isHidden = true
            self.footerView.isHidden = true
        }) { (Bool) in
            UIView.animate(withDuration: 0.5, animations: { 
                self.previewView.isHidden = false
                self.previewView.previewImageView.image = self.capturedImage
                let dayNumber = Calendar.current.dateComponents([.day], from: LapseManager.shared.currentLapse?.creationDate ?? Date(), to: Date()).day! + 1
                self.previewView.dayLabel.text = "\(NSLocalizedString("day", comment: "")) \(dayNumber)"
            }, completion: { (Bool) in
                NSLog("Did move to lapse description nib")
            })
        }
    }
    
    func previewLayaoutAnimation() {
        UIView.animate(withDuration: 1, animations: {
            self.previewView.isHidden = true
        }) { (Bool) in
            UIView.animate(withDuration: 0.5, animations: {
                self.headerView.isHidden = false
                self.footerView.isHidden = false
            }, completion: { (Bool) in
                NSLog("Did move to camera nib")
            })
        }
    }
    
    func setInitialUI() {
        self.previewView.isHidden = true
    }
}
