//
//  EmptyViewController.swift
//  PULSE
//
//  Created by Nikita Savchuk on 06.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//
// EmptyViewControllerID

import UIKit
import Foundation

class EmptyViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupStaticUI()
        self.setupGesture()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupStaticUI() {
        self.view.backgroundColor = Colors.gray
        imageView.image = UIImage(named: "add_lapse_icon")
    }

    func setupGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(moveToCamera))
        self.imageView.addGestureRecognizer(tapGesture)
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func moveToCamera() {
        //MARK: Create new LAPSE for USER
        let cameraVC = storyboard?.instantiateViewController(withIdentifier: "CameraViewControllerID") as? CameraViewController
        present(cameraVC!, animated: true) { 
            NSLog("Did move to camera")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
