//
//  SettingsViewController.swift
//  LAPSE
//
//  Created by valivaxa on 4/2/17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var everydayNotificationsSwitch: UISwitch!
    @IBOutlet weak var yearNotificationsSwitch: UISwitch!
    @IBOutlet weak var allNotificationsSwitch: UISwitch!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lapseNotifications: UILabel!
    @IBOutlet weak var yearNotifications: UILabel!
    @IBOutlet weak var removeNotifications: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setSwitches()
        setupUI()
    }
    
    private func setSwitches() {
        everydayNotificationsSwitch.isOn = NotificationsManager.shared.everydayNotificationsAvailable
        yearNotificationsSwitch.isOn = NotificationsManager.shared.yearNotificationsAvailable
        allNotificationsSwitch.isOn = !(NotificationsManager.shared.everydayNotificationsAvailable || NotificationsManager.shared.yearNotificationsAvailable)
        allNotificationsSwitch.isEnabled = NotificationsManager.shared.everydayNotificationsAvailable || NotificationsManager.shared.yearNotificationsAvailable
    }
    
    private func setupUI() {
        titleLabel.attributedText = titleAttributes()
        lapseNotifications.attributedText = labelsAttributes(string: NSLocalizedString("lapse_notifications", comment: ""))
        yearNotifications.attributedText = labelsAttributes(string: NSLocalizedString("year_notifications", comment: ""))
        removeNotifications.attributedText = labelsAttributes(string: NSLocalizedString("off_notifications", comment: ""))
    }
    
    @IBAction func switchChanged(_ sender: UISwitch) {
        allNotificationsSwitch.isEnabled = everydayNotificationsSwitch.isOn || yearNotificationsSwitch.isOn
        if sender == everydayNotificationsSwitch {
            NotificationsManager.shared.everydayNotificationsAvailable = sender.isOn
        } else if sender == yearNotificationsSwitch {
            NotificationsManager.shared.yearNotificationsAvailable = sender.isOn
        } else {
            if sender.isOn {
                NotificationsManager.shared.everydayNotificationsAvailable = false
                NotificationsManager.shared.yearNotificationsAvailable = false
                everydayNotificationsSwitch.isOn = false
                yearNotificationsSwitch.isOn = false
            }
        }
    }
    
    @IBAction func dismissTap(_ sender: Any) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func labelsAttributes(string: String) -> NSAttributedString {
        let attributes : [String : Any] = [
            NSFontAttributeName : UIFont(name: "ProximaNova-Regular", size: 18)!,
            NSForegroundColorAttributeName : UIColor.black,
            ]
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
    
    func titleAttributes() -> NSAttributedString {
        let string = NSLocalizedString("notifications", comment: "")
        let attributes : [String : Any] = [
            NSFontAttributeName : UIFont(name: "GothamPro-Medium", size: 18)!,
            NSForegroundColorAttributeName : UIColor.black,
            ]
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
}
