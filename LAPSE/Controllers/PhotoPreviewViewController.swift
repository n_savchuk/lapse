//
//  PhotoPreviewViewController.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 22.03.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit
import JMMarkSlider

class PhotoPreviewViewController: UIViewController {

    @IBOutlet weak var slider: JMMarkSlider!
    
    @IBOutlet weak var photoPreviewImageView: UIImageView!
    
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBAction func sliderAction(_ sender: UISlider) {
        if currPhoto != Int(sender.value) && sender.value < sender.maximumValue {
            photoPreviewImageView.image = UIImage(data: (LapseManager.shared.currentLapse?.items[Int(sender.value)].image)!)
            currPhoto = Int(sender.value)
            let dayNumber = Calendar.current.dateComponents([.day], from: LapseManager.shared.currentLapse?.creationDate ?? Date(), to: (LapseManager.shared.currentLapse?.items[Int(sender.value)].date)!).day! + 1
            self.dateLabel.text = "\(NSLocalizedString("DAY", comment: "")) \(dayNumber)"
            self.descriptionLabel.text = LapseManager.shared.currentLapse?.items[Int(sender.value)].photoDescription
        }
    }
    private var currPhoto: Int = 0
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSliderUI()
        photoPreviewImageView.image = UIImage(data: (LapseManager.shared.currentLapse?.items.last?.image)!)
        descriptionLabel.text = LapseManager.shared.currentLapse?.items.last?.photoDescription
        let dayNumber = Calendar.current.dateComponents([.day], from: LapseManager.shared.currentLapse?.creationDate ?? Date(), to: (LapseManager.shared.currentLapse?.items.last?.date)!).day! + 1
        self.dateLabel.text = "\(NSLocalizedString("DAY", comment: "")) \(dayNumber)"
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(lapseLongPressRecognizer(_:)))
        longPress.minimumPressDuration = 1.0
        self.view.addGestureRecognizer(longPress)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isStatusBarHidden = false
        LapseManager.shared.currentLapse = nil
    }
    
    func setupTextUI() {
        
    }
    
    func setupSliderUI() {
        slider.maximumValue = Float(LapseManager.shared.currentLapse?.items.count ?? 1)
        self.slider.markColor = UIColor.white
        
        self.slider.markWidth = setupMarksWidth(for: LapseManager.shared.currentLapse?.items.count ?? 1)
        self.slider.markPositions = setupSliderPosition(for: LapseManager.shared.currentLapse?.items.count ?? 1)
        self.slider.selectedBarColor = UIColor.clear
        self.slider.unselectedBarColor = UIColor.clear
        self.currPhoto = (LapseManager.shared.currentLapse?.items.count ?? 1) - 1
        if LapseManager.shared.currentLapse?.items.count == 1 {
            self.slider.value = 0.5
            self.slider.isUserInteractionEnabled = false
        } else {
            self.slider.value = slider.maximumValue - 0.1
            self.slider.isUserInteractionEnabled = true
        }
        self.slider.handlerImage = UIImage(named: "slider_handler")
        
    }
    
    func setupMarksWidth(for photosCount: Int) -> CGFloat {
        guard photosCount > 0 && photosCount <= 60 else { return 0 }
        let maxWidth = self.slider.frame.width - CGFloat(2 * (photosCount - 1))
        return maxWidth / CGFloat(photosCount)
    }
    
    func setupSliderPosition(for photosCount: Int) -> [Any] {
        guard photosCount > 0 && photosCount <= 60 else { return [] }
        let sliderWidth = self.slider.frame.width
        let markWidth = self.setupMarksWidth(for: photosCount)
        let markCenter = (markWidth * 100 / (sliderWidth - CGFloat(2 * (photosCount - 1)))) / 2
        var positions = [Any]()
        for i in 0..<photosCount {
            let position = markCenter + (CGFloat(i) * (2) + CGFloat(i) * markWidth) * 100 / sliderWidth
            positions.append(position)
        }
        return positions
    }
    
    
    @objc
    func lapseLongPressRecognizer(_ recognizer: UILongPressGestureRecognizer) {
        deletePhoto()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if #available(iOS 9.0, *) {
                if traitCollection.forceTouchCapability == .available {
                    if touch.force >= touch.maximumPossibleForce {
                        deletePhoto()
                    }
                }
            }
        }
    }
    
    private func deletePhoto() {
        let vc = UIAlertController(title: NSLocalizedString("photo_delete_title", comment: ""),
                                   message: nil, preferredStyle: .actionSheet)
        vc.addAction(UIAlertAction(title: NSLocalizedString("keep_photo", comment: ""), style: .default, handler: nil))
        vc.addAction(UIAlertAction(title: NSLocalizedString("delete", comment: ""), style: .destructive, handler: { _ in
            LapseManager.shared.deletePhoto(atIndex: self.currPhoto)
            if LapseManager.shared.currentLapse!.items.isEmpty {
                LapseManager.shared.deleteLapse(lapse: LapseManager.shared.currentLapse, lapseDeleteCompletion: {
                    self.performSegue(withIdentifier: "ToPagerSegue", sender: nil)
                })
            } else {
                self.setupSliderUI()
                self.photoPreviewImageView.image = UIImage(data: (LapseManager.shared.currentLapse?.items[Int(self.slider.maximumValue / 2)].image)!)
                
            }
        }))
        vc.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil))
        self.present(vc, animated: true, completion: nil)
    }
}
