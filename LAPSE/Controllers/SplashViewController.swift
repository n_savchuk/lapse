//
//  ViewController.swift
//  PULSE
//
//  Created by Nikita Savchuk on 02.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit
import Foundation

class SplashViewController: UIViewController {

    var counter: Int = 0 {
        didSet {
            let fractionalProgress = Float(counter) / 100.0
            let animated = counter != 0
            
            rightProgressView.setProgress(fractionalProgress, animated: animated)
            leftProgressView.setProgress(fractionalProgress, animated: animated)
        }
    }
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var leftProgressView: UIProgressView!
    @IBOutlet weak var rightProgressView: UIProgressView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        leftProgressView.setProgress(0, animated: true)
        rightProgressView.setProgress(0, animated: true)
        let tap = UITapGestureRecognizer(target: self, action: #selector(startCount))
        self.view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupProgressView()
    }

    @objc func startCount() {
        self.counter = 0
        for _ in 0..<100 {
            DispatchQueue.global(qos: .background).async {
                sleep(2)
                DispatchQueue.main.async {
                    self.counter += 1
                    return
                }
            }
        }
    }
    
    func setupProgressView() {
        leftProgressView.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        leftProgressView.isHidden = true
        rightProgressView.isHidden = true
    }
    
//    func drawStripes(context: CGContext, inRect: CGRect) {
//        context.saveGState()
//        let path = UIBezierPath(roundedRect: inRect, cornerRadius: self.bor)
//    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

