//
//  ContentViewController.swift
//  PULSE
//
//  Created by Nikita Savchuk on 06.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//
// ContentViewControllerID

import UIKit

@objc protocol LapseContentControllerDelegate: class {
    @objc func contentControllerDidPressDelete(_ contentViewController: ContentViewController)
//    @objc func contentControllerDidPressDelete(_ contentViewController: ContentViewController)
}

class ContentViewController: UIViewController {

    weak var delegate: LapseContentControllerDelegate?
    
    var pageIndex: Int!
    var lapse: Lapse?
    
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var lapseImageView: UIImageView!
    
    @IBAction func addButtonAction(_ sender: UIButton) {
        LapseManager.shared.currentLapse = lapse
        let cameraVC = storyboard?.instantiateViewController(withIdentifier: "CameraViewControllerID") as! CameraViewController
        LapseManager.shared.currentLapse = lapse
        self.present(cameraVC, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupStaticUI()
        let tap = UITapGestureRecognizer(target: self, action: #selector(lapseTapRecognizer(_:)))
        self.view.addGestureRecognizer(tap)
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(lapseLongPressRecognizer(_:)))
        longPress.minimumPressDuration = 1
        self.view.addGestureRecognizer(longPress)
        self.view.frame.size.width -= 100
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.setupDynamicUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    func setupStaticUI() {
        self.lapseImageView.layer.cornerRadius = 1.5
        self.lapseImageView.clipsToBounds = true
    }

    func setupDynamicUI() {
        self.lapseImageView.image = UIImage(data: (self.lapse?.items.last?.image!)!)
    }
    
    @objc
    func lapseTapRecognizer(_ recognizer: UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "photoPreviewViewControllerID")
        LapseManager.shared.currentLapse = lapse
        self.present(vc!, animated: true, completion: nil)
    }
    
    @objc
    func lapseLongPressRecognizer(_ recognizer: UILongPressGestureRecognizer) {
        deleteLapse()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if #available(iOS 9.0, *) {
                if traitCollection.forceTouchCapability == .available {
                    if touch.force >= touch.maximumPossibleForce {
                        deleteLapse()
                    }
                }
            }
        }
    }
    
    private func deleteLapse() {
        let vc = UIAlertController(title: "Delete lapse", message: "Are you sure that you want to delete lapse?", preferredStyle: .alert)
        vc.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { _ in
            LapseManager.shared.deleteLapse(lapse: self.lapse, lapseDeleteCompletion: {
                self.delegate?.contentControllerDidPressDelete(self)
            })
        }))
        vc.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(vc, animated: true, completion: nil)
    }
}
