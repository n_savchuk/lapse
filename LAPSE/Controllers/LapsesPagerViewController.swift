//
//  LapsesPagerViewController.swift
//  LAPSE
//
//  Created by valivaxa on 4/2/17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit
import FSPagerView
import RealmSwift
import SideMenu
import Hero
import RealmSwift
import SwiftyStoreKit

class LapsesPagerViewController: UIViewController {

    @IBOutlet weak var pagerView: FSPagerView! {
        didSet{
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        }
    }
    var lapses: AnyRealmCollection<Lapse>!
    
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var pageControl: FSPageControl!
    @IBOutlet weak var adbiceLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pagerView.register(UINib.init(nibName: "LapseCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell_ID")
        pagerView.register(UINib.init(nibName: "LapseCollectionEmptyCell", bundle: nil), forCellWithReuseIdentifier: "emptyCell_ID")
        NotificationCenter.default.addObserver(self, selector: #selector(setupUIForUser), name: NSNotification.Name("userDidLogin"), object: nil)
        setupUIForUser()
        self.isHeroEnabled = true
        self.setMenuButtonUI()
        self.setupSideMenu()
        self.setupPager()
        self.setHeaderLabel()
    }

    //TODO: Setup Font
    private func setHeaderLabel() {
        headerLabel.attributedText = titleAttributes()
    }
    
    func reloadData() {
        pagerView.reloadData()
        if lapses.count > 0 {
            pageControl.isHidden = false
            adbiceLabel.isHidden = true
            pageControl.numberOfPages = lapses.count + 1
        } else {
            pageControl.isHidden = true
            adbiceLabel.isHidden = false
        }
    }
    
    private func setMenuButtonUI() {
        menuButton.heroID = "userImage"
        menuButton.heroModifiers = [.translate()]
        if UserManager.shared.userIsLoggined() {
            menuButton.setImage(UIImage(data: (UserManager.shared.getUser()?.photo!)!), for: UIControlState())
        } else {
            menuButton.setImage(UIImage(named: "empty_user")!, for: UIControlState())
        }
        menuButton.clipsToBounds = true
        menuButton.layer.cornerRadius = menuButton.frame.height / 2
    }
    
    private func setupSideMenu() {
        let leftMenuVC = UISideMenuNavigationController(rootViewController: SideMenuTableViewController.shared)
        leftMenuVC.leftSide = true
        SideMenuManager.menuWidth = self.view.frame.width * 0.85
        SideMenuManager.menuAnimationPresentDuration = 0.1
        SideMenuManager.menuPresentMode = .viewSlideInOut
        SideMenuManager.menuFadeStatusBar = false
        SideMenuManager.menuShadowColor = UIColor.clear
        
    }
    private func setupPager() {
        self.adbiceLabel.attributedText = self.adbiseAttributedText()
        pageControl.itemSpacing = 10
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(lapseLongPressRecognizer(_:)))
        longPress.minimumPressDuration = 1.0
        pagerView.addGestureRecognizer(longPress)
        pageControl.numberOfPages = lapses.count + 1
        self.pagerView.itemSize = self.pagerView.frame.size.applying(CGAffineTransform(scaleX: 0.8, y: 0.9))
        self.pagerView.interitemSpacing = 20
        pageControl.setImage(UIImage(named: "unselected_dot"), for: .normal)
        pageControl.setImage(UIImage(named: "selected_dot"), for: .selected)
        
        reloadData()
    }
    
    @objc func setupUIForUser() {
        
        if UserManager.shared.userIsLoggined() {
            menuButton.setImage(UIImage(data: (UserManager.shared.getUser()?.photo!)!), for: UIControlState())
        } else {
            menuButton.setImage(UIImage(named: "empty_user")!, for: UIControlState())
        }
        
        if let lapses = UserManager.shared.getUser()?.lapses {
            self.lapses = AnyRealmCollection<Lapse>(lapses)
        } else {
            let realm = try! Realm()
            let unkownUserLapses = realm.object(ofType: User.self, forPrimaryKey: "-1")!.lapses
            self.lapses = AnyRealmCollection<Lapse>(unkownUserLapses)
        }
        self.reloadData()
    }
    
    @objc func addButtonPressed(_ sender: UIButton) {
        guard pagerView.currentIndex < lapses.count else { return }
        LapseManager.shared.currentLapse = lapses[pagerView.currentIndex]
        let cameraVC = storyboard?.instantiateViewController(withIdentifier: "CameraViewControllerID") as! CameraViewController
        self.present(cameraVC, animated: true, completion: nil)
    }
    
    @objc func extendButtonPressed(_ sender: UIButton) {
        let extendView = ProlongVersionView(frame: (self.navigationController?.view.bounds)!)
        extendView.alpha = 0
        self.navigationController?.view.addSubview(extendView)
        UIView.animate(withDuration: 0.7) { 
            extendView.alpha = 1
        }
        extendView.cancelButtonTapHandler {
            UIView.animate(withDuration: 0.7, animations: { 
                extendView.alpha = 0.0
            })
            extendView.removeFromSuperview()
        }
        extendView.extendButtonTapHandler {
            self.purchaseProVersion(completion: {
                UIView.animate(withDuration: 0.7, animations: {
                    extendView.alpha = 0.0
                })
                extendView.removeFromSuperview()
            })
        }
    }

    private func purchaseProVersion(completion: @escaping (Void) ->()) {
        if SwiftyStoreKit.canMakePayments {
            SwiftyStoreKit.purchaseProduct("lapse_premium_version", atomically: true, applicationUsername: "com.MS.LAPSE", completion: { (result) in
                switch result {
                case .success(product: let product):
                    UserManager.shared.getUser()?.hasMadePayment = true
                    completion()
                case .error(error: let error):
                    print(error.localizedDescription)
                    switch error.code {
                    case .unknown: print("Unknown error")
                    case .clientInvalid: print("Not allowed to make payment")
                    case .paymentCancelled: break
                    case .paymentInvalid: print("The purchase identifier was invalid")
                    case .paymentNotAllowed: print("The device is not allowed to make the payment")
                    case .storeProductNotAvailable: print("The product is not available in the current storefront")
                    case .cloudServicePermissionDenied: print("Access to cloud service information is not allowed")
                    case .cloudServiceNetworkConnectionFailed: print("Could not connect to the network")
                    default:
                        break
                    }
                }
            })
        }
    }
}

extension LapsesPagerViewController: FSPagerViewDataSource, FSPagerViewDelegate {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return lapses.count + 1
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        if index < lapses.count {
            let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell_ID", at: index) as! LapseCollectionViewCell
            cell.lapseImageView.image = UIImage(data: lapses[index].items.last!.image!)
            cell.lapseImageView?.layer.cornerRadius = 2
            cell.lapseImageView?.clipsToBounds = true
            cell.layer.shadowColor = UIColor(red: 9/255, green: 12/255, blue: 12/255, alpha: 1).cgColor
            cell.layer.shadowRadius = 8
            cell.layer.shadowOpacity = 0.5
            cell.layer.shadowOffset = CGSize(width: 0, height: 8)
            if lapses[index].items.count >= 21 && UserManager.shared.getUser()?.hasMadePayment == false {
                cell.addLapseItemButton.isHidden = true
                cell.extendButton.isHidden = false
                cell.extendButton.addTarget(self, action: #selector(extendButtonPressed(_:)), for: .touchUpInside)
                cell.extendButton.backgroundColor = UIColor.clear
                cell.extendButton.layer.borderColor = UIColor(red: 246/255, green: 166/255, blue: 35/255, alpha: 1).cgColor
                cell.extendButton.layer.borderWidth = 1
                cell.extendButton.layer.cornerRadius = 3
                cell.extendButton.setAttributedTitle(extendAttributes(), for: UIControlState())
                cell.bringSubview(toFront: cell.extendButton)
            } else {
                cell.addLapseItemButton.isHidden = false
                cell.extendButton.isHidden = true
                cell.addLapseItemButton.addTarget(self, action: #selector(addButtonPressed(_:)), for: .touchUpInside)
                cell.bringSubview(toFront: cell.addLapseItemButton)
            }
            return cell
        } else {
            let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "emptyCell_ID", at: index) as! LapseCollectionEmptyCell
            return cell
        }
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        self.pageControl.currentPage = index
        if index < lapses.count {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "photoPreviewViewControllerID")
            LapseManager.shared.currentLapse = lapses[index]
            self.present(vc!, animated: true, completion: nil)
        } else {
            let cameraVC = storyboard?.instantiateViewController(withIdentifier: "CameraViewControllerID") as? CameraViewController
            self.present(cameraVC!, animated: true) {
                NSLog("Did move to camera")
            }
        }
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        guard self.pageControl.currentPage != pagerView.currentIndex else {
            return
        }
        self.pageControl.currentPage = pagerView.currentIndex // Or Use KVO with property "currentIndex"
    }
    
    @objc
    func lapseLongPressRecognizer(_ recognizer: UILongPressGestureRecognizer) {
        deleteLapse()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if #available(iOS 9.0, *) {
                if traitCollection.forceTouchCapability == .available {
                    if touch.force >= touch.maximumPossibleForce {
                        deleteLapse()
                    }
                }
            }
        }
    }
    
    private func deleteLapse() {
        if pagerView.currentIndex < lapses.count {
            let vc = UIAlertController(title: NSLocalizedString("lapse_delete_title", comment: ""),
                                       message: nil, preferredStyle: .actionSheet)
            vc.addAction(UIAlertAction(title: NSLocalizedString("keep_lapse", comment: ""), style: .default, handler: nil))
            vc.addAction(UIAlertAction(title: NSLocalizedString("delete", comment: ""), style: .destructive, handler: { _ in
                LapseManager.shared.deleteLapse(lapse: self.lapses[self.pagerView.currentIndex], lapseDeleteCompletion: { self.reloadData()
                })
            }))
            vc.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil))
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func toLapsesPager(_ segue: UIStoryboardSegue) {
        self.reloadData()
    }
    
    func adbiseAttributedText() -> NSAttributedString {
        let string = NSLocalizedString("home_add_lapse", comment: "")
        let attributes : [String : Any] = [
            NSFontAttributeName : UIFont(name: "GothamPro-Medium", size: 18)!,
            NSForegroundColorAttributeName : UIColor.black,
            ]
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
    
    func titleAttributes() -> NSAttributedString {
        let string = NSLocalizedString("my_lapses", comment: "")
        let attributes : [String : Any] = [
            NSFontAttributeName : UIFont(name: "GothamPro-Medium", size: 18)!,
            NSForegroundColorAttributeName : UIColor.black,
            ]
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
    
    func extendAttributes() -> NSAttributedString {
        let string = NSLocalizedString("extend_button", comment: "")
        let attributes : [String : Any] = [
            NSFontAttributeName : UIFont(name: "GothamPro-Medium", size: 17)!,
            NSForegroundColorAttributeName : UIColor(red: 246/255, green: 166/255, blue: 35/255, alpha: 1),
            ]
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
}


