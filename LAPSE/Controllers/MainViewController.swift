//
//  MainViewController.swift
//  PULSE
//
//  Created by Nikita Savchuk on 06.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit
import Foundation
import SideMenu
import Hero
import RealmSwift

class MainViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var pageViewController: UIPageViewController!
    
    var lapses: AnyRealmCollection<Lapse>!
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var headerLabel: UILabel!
    
    
    @IBAction func didTapMenuButton(_ sender: UIButton) {}

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(setupUIForUser), name: NSNotification.Name("userDidLogin"), object: nil)
//        if UserManager.shared.userIsLoggined() {
            setupUIForUser()
//        } else {
//            let alert = UIAlertController(title: "title", message: "you should login first", preferredStyle: .alert)
//            let alertAction = UIAlertAction(title: "ok", style: .default, handler: { (action) in
//                NSLog("should open menu")
//            })
//            alert.addAction(alertAction)
//            present(alert, animated: true, completion: nil)
//        }
        self.isHeroEnabled = true
        
        self.setMenuButtonUI()
        self.setupSideMenu()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setMenuButtonUI()
    }
    
    //TODO: Setup Font
    private func setHeaderLabel() {
        let headerText = NSLocalizedString("LAPSE", comment: "")
        headerLabel.text = headerText
    }
    
    private func setMenuButtonUI() {
        menuButton.heroID = "userImage"
        menuButton.heroModifiers = [.translate()]
        if UserManager.shared.userIsLoggined() {
            menuButton.setImage(UIImage(data: (UserManager.shared.getUser()?.photo!)!), for: UIControlState())
        } else {
            menuButton.setImage(UIImage(named: "empty_user")!, for: UIControlState())
        }
        menuButton.imageView?.contentMode = .center
        menuButton.clipsToBounds = true
        menuButton.layer.cornerRadius = menuButton.frame.height / 2
    }
    
    private func setupSideMenu() {
        let leftMenuVC = UISideMenuNavigationController(rootViewController: SideMenuTableViewController.shared)
        leftMenuVC.leftSide = true
        SideMenuManager.menuWidth = self.view.frame.width * 0.85 //не отработал
        SideMenuManager.menuAnimationPresentDuration = 0.1
        SideMenuManager.menuPresentMode = .viewSlideInOut //не отработал
        SideMenuManager.menuFadeStatusBar = false
        
    }

    //MARK: – Page View Controller Data Source
    
    @objc func setupUIForUser() {
        if let lapses = UserManager.shared.getUser()?.lapses {
            self.lapses = AnyRealmCollection<Lapse>(lapses)
        } else {
            self.lapses = AnyRealmCollection<Lapse>(try! Realm().objects(Lapse.self))
        }
        self.setupPageViewController()
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if let vc = viewController as? ContentViewController {
            var index: Int = vc.pageIndex as Int
            
            if (index == 0 || index == NSNotFound) {
                return nil
            }
            
            index -= 1
            return self.viewControllerAtIndex(index: index)
            
        } else if viewController is EmptyViewController {
            if lapses.count > 0 {
                return viewControllerAtIndex(index: lapses.count - 1)
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if let vc = viewController as? ContentViewController {
        
            var index: Int = vc.pageIndex as Int
            
            if (index == NSNotFound) {
                return nil
            }
            
            index += 1
            
            if (index == self.lapses.count) {
                let emptyVC = storyboard?.instantiateViewController(withIdentifier: "EmptyViewControllerID")
                return emptyVC
            }
            
            return self.viewControllerAtIndex(index: index)
            
        }
        return nil
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return self.lapses.count + 1
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    func viewControllerAtIndex(index: Int) -> ContentViewController {
        if ((lapses.count == 0) || (index >= self.lapses.count)) {
            return ContentViewController()
        }
        
        let vc: ContentViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContentViewControllerID") as! ContentViewController
        vc.pageIndex = index
        vc.delegate = self
        vc.lapse = self.lapses[index]
        return vc
    }
    
    func setupPageViewController() {
        pageViewController = self.storyboard?.instantiateViewController(withIdentifier: "PageViewControllerID") as! UIPageViewController
        pageViewController.dataSource = self
        
        if UserManager.shared.userIsLoggined() && (UserManager.shared.getUser()?.lapses.count)! > 0 ||
            (try! Realm().objects(Lapse.self).count) > 0 {
            let startVC = self.viewControllerAtIndex(index: 0) as ContentViewController
            self.pageViewController.setViewControllers([startVC], direction: .forward, animated: true, completion: nil)
        } else {
            let emptyVC = storyboard?.instantiateViewController(withIdentifier: "EmptyViewControllerID")
            self.pageViewController.setViewControllers([emptyVC!], direction: .forward, animated: true, completion: nil)
        }
        self.pageViewController.view.frame = CGRect(x: 0, y: 20, width: self.view.frame.width, height: self.view.frame.height - 67)
        
        self.addChildViewController(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.didMove(toParentViewController: self)
    }
    
    @IBAction func unwindToMain(_ sender: UIStoryboardSegue) { }
}

class CustomImagePageControl: UIPageControl {
    
    let selectedDot = UIImage(named: "selected_dot")!
    let unselectedDot = UIImage(named: "unselected_dot")!
    let selectedPlus = UIImage(named: "selected_plus")!
    let unselectedPlus = UIImage(named: "unselected_plus")!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.pageIndicatorTintColor = UIColor.clear
        self.currentPageIndicatorTintColor = UIColor.clear
        self.clipsToBounds = false
    }
    
    func updateDots() {
        var i = 0
        for view in self.subviews {
            if let imageView = self.imageForSubview(view) {
                if i == self.currentPage {
                    imageView.image = self.selectedDot
                } else if i == self.subviews.count {
                    imageView.image = self.unselectedPlus
                } else {
                    imageView.image = self.unselectedDot
                }
                i = i + 1
            } else {
                var dotImage = self.unselectedDot
                if i == self.currentPage {
                    dotImage = self.selectedDot
                } else if i == self.currentPage && i == self.subviews.count {
                    dotImage = self.selectedPlus
                }
                view.clipsToBounds = false
                view.addSubview(UIImageView(image:dotImage))
                i = i + 1
            }
        }
    }
    
    fileprivate func imageForSubview(_ view:UIView) -> UIImageView? {
        var dot:UIImageView?
        
        if let dotImageView = view as? UIImageView {
            dot = dotImageView
        } else {
            for foundView in view.subviews {
                if let imageView = foundView as? UIImageView {
                    dot = imageView
                    break
                }
            }
        }
        
        return dot
    }
}

extension MainViewController: LapseContentControllerDelegate {
    func contentControllerDidPressDelete(_ contentViewController: ContentViewController) {
//        let index = contentViewController.pageIndex
//        if index < lapses.count {
//            self.pageViewController.setViewControllers([viewControllerAtIndex(index: index)], direction: <#T##UIPageViewControllerNavigationDirection#>, animated: <#T##Bool#>, completion: <#T##((Bool) -> Void)?##((Bool) -> Void)?##(Bool) -> Void#>)
        let emptyVC = storyboard?.instantiateViewController(withIdentifier: "EmptyViewControllerID")
        self.pageViewController.setViewControllers([emptyVC!], direction: .forward, animated: true, completion: nil)
    }
}
