//
//  SideMenuTableViewController.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 10.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//
//SideMenuNavigationControllerID
//SideMenuTableViewControllerID

import UIKit
import Foundation
import SideMenu
import Hero
import MessageUI
import SafariServices

class SideMenuTableViewController: UITableViewController, MFMailComposeViewControllerDelegate {
    
    static let shared = SideMenuTableViewController()

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var footerView: UIView!
    
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var lapsesNumberLabel: UILabel!
    
    @IBOutlet weak var authorizationButton: UIButton!
    
    @IBAction func authorizationAction(_ sender: UIButton) {
        if UserManager.shared.userIsLoggined() {
            let alert = UIAlertController(title: NSLocalizedString("logout_title", comment: ""), message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: NSLocalizedString("logout_stay", comment: ""), style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: NSLocalizedString("logout_leave", comment: ""), style: .destructive, handler: { _ in
                UserManager.shared.logout()
                self.setupDynamicUI()
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            UserManager.shared.login(from: self, completion: {
                DispatchQueue.main.async {
                    self.setupDynamicUI()
                }
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isHeroEnabled = true
        self.setupStaticUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.setupDynamicUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 3
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuTableViewCell

            switch indexPath.row {
            case 0:
                cell.menuItemLabel.attributedText = Fonts.menuItems(string: NSLocalizedString("push_notification_button", comment: ""))
            case 1:
                cell.menuItemLabel.attributedText = Fonts.menuItems(string: NSLocalizedString("report_a_problem_button", comment: ""))
            case 2:
                cell.menuItemLabel.attributedText = Fonts.menuItems(string: NSLocalizedString("conditions", comment: ""))
            default: ()
            }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsController")
            self.navigationController?.pushViewController(vc!, animated: true)
        case 1:
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients(["thelapseapp@gmail.com"])
                mail.setSubject(NSLocalizedString("mail_subject", comment: ""))
                present(mail, animated: true)
            } else {
                // show failure alert
            }
        case 2:
            let url = URL(string: "http://www.lapse.pro")
            let svc = SFSafariViewController(url: url!)
            present(svc, animated: true, completion: nil)
        default: ()
        }
    }
    
    fileprivate func setupStaticUI() {
        userImageView.heroID = "userImage"
        userImageView.heroModifiers = [.translate()]
        userImageView.layer.cornerRadius = userImageView.frame.width / 2
        userImageView.contentMode = .scaleToFill
        userImageView.clipsToBounds = true
        
        
        
        tableView.isScrollEnabled = false
        tableView.showsVerticalScrollIndicator = false
    }

    fileprivate func setupDynamicUI() {
        if UserManager.shared.userIsLoggined() {
            userNameLabel.attributedText = Fonts.userName(string: (UserManager.shared.getUser()?.fullName)!)
            guard let lapsesCount = UserManager.shared.getUser()?.lapses.count else {
                return
            }
            if lapsesCount == 0 {
                lapsesNumberLabel.attributedText = Fonts.lapseNumer(string: "\(String(describing: lapsesCount)) \(NSLocalizedString("0_lapses", comment: ""))")
            } else if lapsesCount == 1 {
                lapsesNumberLabel.attributedText = Fonts.lapseNumer(string: "\(String(describing: lapsesCount)) \(NSLocalizedString("1_lapse", comment: ""))")
            } else if lapsesCount >= 2 && lapsesCount <= 4 {
                lapsesNumberLabel.attributedText = Fonts.lapseNumer(string: "\(String(describing: lapsesCount)) \(NSLocalizedString("2-4_lapses", comment: ""))")
            } else {
                lapsesNumberLabel.attributedText = Fonts.lapseNumer(string: "\(String(describing: lapsesCount)) \(NSLocalizedString("4+lapses", comment: ""))")
            }
            userImageView.image = UIImage(data: (UserManager.shared.getUser()?.photo!)!)
            authorizationButton.setAttributedTitle(Fonts.facebookLogout(string: NSLocalizedString("logout", comment: "")), for: UIControlState())
        } else {
            userNameLabel.attributedText = Fonts.userName(string: NSLocalizedString("guest_name_placeholder", comment: ""))
            lapsesNumberLabel.attributedText = Fonts.lapseNumer(string: NSLocalizedString("guest_lapse_placeholder", comment: ""))
            userImageView.image = UIImage(named: "avaEmpty")!
            authorizationButton.setAttributedTitle(Fonts.facebookLogin(string: NSLocalizedString("login", comment: "")), for: UIControlState())
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
         controller.dismiss(animated: true)
    }

}
