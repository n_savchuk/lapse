//
//  LapseItem.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 09.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit
import RealmSwift

class LapseItem: Object {

    dynamic var image: Data?
    dynamic var date: Date?
    dynamic var photoDescription: String?
}
