//
//  User.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 09.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class User: Object {
    
    dynamic var fullName: String = ""
    dynamic var photoString: String = ""
    dynamic var email: String = ""
    dynamic var accessToken: String = ""
    dynamic var photo: Data?
    dynamic var id: String = UUID().uuidString
    dynamic var isActive = false
    dynamic var hasMadePayment = false
    
    let lapses = List<Lapse>()
    
    override static func primaryKey() -> String? {
        return "id"
    }

}
