//
//  Lapse.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 09.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit
import RealmSwift

class Lapse: Object {
    dynamic var creationDate: Date = Date()
    let items = List<LapseItem>()
}
