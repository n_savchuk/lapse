//
//  CameraHeaderView.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 14.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit
import Foundation

class LapsesHeaderView: UIView {

    @IBOutlet weak var gradientImageView: UIImageView!
    
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBAction func closeButtonAction(_ sender: UIButton) {

    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
