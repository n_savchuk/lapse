//
//  LapseCollectionViewCell.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 10.04.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit
import FSPagerView

class LapseCollectionViewCell: FSPagerViewCell {
    
    @IBOutlet weak var lapseImageView: UIImageView!
    @IBOutlet weak var lapseMaskView: UIView!
    @IBOutlet weak var addLapseItemButton: UIButton!
    @IBOutlet weak var extendButton: UIButton!
}
