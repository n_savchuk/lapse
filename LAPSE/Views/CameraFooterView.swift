//
//  CameraFooterView.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 15.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit
import Foundation

class CameraFooterView: UIView {
    
    var mainView: UIView!

    @IBOutlet weak var gradientImageView: UIImageView!

    @IBOutlet weak var capturePhotoButton: UIButton!
    
    @IBAction func captureButtonAction(_ sender: UIButton) {
        capturePhotoButtonHandler?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        fromXib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        fromXib()
    }
    
    private func fromXib() {
        self.mainView = loadViewFromNib()
        mainView.frame = bounds
        mainView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        setupUI()
        addSubview(mainView)
    }
    
    private func loadViewFromNib() -> UIView {
        let bunlde = Bundle(for: type(of: self))
        let view = bunlde.loadNibNamed("CameraFooterView", owner: self, options: nil)![0] as! UIView
        return view
    }
    
    private func setupUI() {
        self.capturePhotoButton.tintColor = Colors.white
        self.capturePhotoButton.setImage(UIImage(named: "take_photo_icon"), for: UIControlState())
        self.capturePhotoButton.layer.cornerRadius = capturePhotoButton.frame.size.width / 2
        self.gradientImageView.image = UIImage(named: "footer_gradient")!
    }
    
    //MARK: Button Tap Handler
    
    fileprivate var capturePhotoButtonHandler: (() -> Void)?
    
    func setCapturePhotoButtonTapHandler(_ handler: @escaping () -> Void) {
        self.capturePhotoButtonHandler = handler
    }
}
