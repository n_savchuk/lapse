//
//  ProlongVersionView.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 4/25/17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit

class ProlongVersionView: UIView {

    var mainView: UIView!
    
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var extendButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    
    @IBAction func extendButtonAction(_ sender: UIButton) {
        extendButtonHandler?()
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        cancelButtonHandler?()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        fromXib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        fromXib()
    }
    
    private func fromXib() {
        self.mainView = loadViewFromNib()
        mainView.frame = bounds
        mainView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        setupUI()
        addSubview(mainView)
    }
    
    private func loadViewFromNib() -> UIView {
        let bunlde = Bundle(for: type(of: self))
        let view = bunlde.loadNibNamed("ProlongVersionView", owner: self, options: nil)![0] as! UIView
        return view
    }
    
    private func setupUI() {
        self.bannerView.layer.cornerRadius = 2
        self.mainView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.messageLabel.attributedText = messageAttributes()
        self.priceLabel.attributedText = priceAttributes()
        self.extendButton.setAttributedTitle(extendAttributes(), for: UIControlState())
        self.extendButton.layer.borderWidth = 2
        self.extendButton.layer.borderColor = UIColor.black.cgColor
        self.extendButton.layer.cornerRadius = 2
        
        self.cancelButton.setAttributedTitle(cancelAttributes(), for: UIControlState())
    }
    
    //MARK: Button Tap Handler
    
    fileprivate var extendButtonHandler: (() -> Void)?
    fileprivate var cancelButtonHandler: (() -> Void)?
    
    
    func extendButtonTapHandler(_ handler: @escaping () -> Void) {
        self.extendButtonHandler = handler
    }
    
    func cancelButtonTapHandler(_ handler: @escaping () -> Void) {
        self.cancelButtonHandler = handler
    }
    
    func messageAttributes() -> NSAttributedString {
        let string = NSLocalizedString("extend_message", comment: "")
        let attributes : [String : Any] = [
            NSFontAttributeName : UIFont(name: "ProximaNova-Regular", size: 16)!,
            NSForegroundColorAttributeName : UIColor.white
            ]
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
    
    func priceAttributes() -> NSAttributedString {
        let string = NSLocalizedString("$0.99", comment: "")
        let attributes : [String : Any] = [
            NSFontAttributeName : UIFont(name: "GothamPro-Medium", size: 35)!,
            NSForegroundColorAttributeName : UIColor.white
            ]
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
    
    func extendAttributes() -> NSAttributedString {
        let string = NSLocalizedString("extend_button", comment: "").uppercased()
        let attributes : [String : Any] = [
            NSFontAttributeName : UIFont(name: "ProximaNova-Regular", size: 18)!,
            NSForegroundColorAttributeName : UIColor.black
            ]
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
    
    func cancelAttributes() -> NSAttributedString {
        let string = NSLocalizedString("cancel", comment: "")
        let attributes : [String : Any] = [
            NSFontAttributeName : UIFont(name: "ProximaNova-Regular", size: 18)!,
            NSForegroundColorAttributeName : UIColor(red: 182/255, green: 110/255, blue: 17/255, alpha: 1),
            ]
        let attributedString = NSAttributedString(string: string, attributes: attributes)
        return attributedString
    }
}
