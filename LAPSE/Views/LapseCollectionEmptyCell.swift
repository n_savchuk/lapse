//
//  LapseCollectionEmptyCell.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 10.04.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit
import FSPagerView

class LapseCollectionEmptyCell: FSPagerViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    
}
