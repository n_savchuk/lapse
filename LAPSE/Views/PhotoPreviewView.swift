//
//  PhotoPreviewView.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 21.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit

class PhotoPreviewView: UIView {

    var mainView: UIView!
    
    var placeholder: String? {
        didSet {
            if let placeholder = placeholder {
                descriptionTexField.attributedPlaceholder = Fonts.lapseItemDescriptionPlaceholder(string: placeholder)
            }
        }
    }
    
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var descriptionTexField: UITextField!
    @IBOutlet weak var retakePhotoButton: UIButton!
 
    
    @IBAction func closeButtonAction(_ sender: UIButton) {
        closeButtonHandler?()
    }
    
    @IBAction func confirmButtonAction(_ sender: UIButton) {
        confirmButtonHandler?()
    }
    
    @IBAction func doneButtonAction(_ sender: UIButton) {
        doneButtonHandler?()
    }
    
    @IBAction func retakePhotoButtonAction(_ sender: UIButton) {
        retakePhotoButtonHandler?()
    }
    
    @IBAction func editingDidBegin(_ sender: UITextField) {
        self.placeholder = ""
        doneButton.isHidden = false
        confirmButton.isHidden = true
        closeButton.isHidden = true
    }
    
    @IBAction func editingDidEnd(_ sender: UITextField) {
        if sender.text?.characters.count == 0 {
            placeholder = NSLocalizedString("description", comment: "")
        }
        doneButton.isHidden = true
        confirmButton.isHidden = false
        closeButton.isHidden = false
    }
  
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        fromXib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        fromXib()
    }
    
    private func fromXib() {
        self.mainView = loadViewFromNib()
        mainView.frame = bounds
        mainView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        setupUI()
        addSubview(mainView)
    }
    
    private func loadViewFromNib() -> UIView {
        let bunlde = Bundle(for: type(of: self))
        let view = bunlde.loadNibNamed("PhotoPreviewView", owner: self, options: nil)![0] as! UIView
        return view
    }
    
    private func setupUI() {
        
        let view = UIView(frame: previewImageView.bounds)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.56)
        previewImageView.addSubview(view)
        
        self.placeholder = NSLocalizedString("description", comment: "")
        self.descriptionTexField.borderStyle = .none
        self.descriptionTexField.backgroundColor = UIColor.clear
        self.descriptionTexField.font = Fonts.lapseItemDescriptionFont
        self.descriptionTexField.textColor = UIColor.white
        self.descriptionTexField.tintColor = UIColor.white
        self.descriptionTexField.autocapitalizationType = .sentences
        self.descriptionTexField.keyboardAppearance = .dark
        self.closeButton.tintColor = Colors.white
        self.confirmButton.tintColor = Colors.white
        self.doneButton.titleLabel?.font = Fonts.lapseItemDescriptionFont
        self.doneButton.setTitle(NSLocalizedString("done", comment: ""), for: UIControlState())
        self.doneButton.tintColor = Colors.white
        self.retakePhotoButton.tintColor = Colors.white
        
        self.closeButton.setImage(UIImage(named: "close_icon"), for: UIControlState())
        self.confirmButton.setImage(UIImage(named: "confirm_icon"), for: UIControlState())
        self.retakePhotoButton.setImage(UIImage(named: "retake_photo_icon"), for: UIControlState())
        
        self.retakePhotoButton.layer.cornerRadius = retakePhotoButton.frame.size.width / 2
        
        self.doneButton.isHidden = true
        self.confirmButton.isHidden = false
    }
    
    //MARK: Button Tap Handler
    
    fileprivate var retakePhotoButtonHandler: (() -> Void)?
    fileprivate var closeButtonHandler: (() -> Void)?
    fileprivate var confirmButtonHandler: (() -> Void)?
    fileprivate var doneButtonHandler: (() -> Void)?
    
    func retakePhotoButtonTapHandler(_ handler: @escaping () -> Void) {
        self.retakePhotoButtonHandler = handler
    }
    
    func closeButtonTapHandler(_ handler: @escaping () -> Void) {
        self.closeButtonHandler = handler
    }
    
    func confirmButtonTapHandler(_ handler: @escaping () -> Void) {
        self.confirmButtonHandler = handler
    }
    
    func doneButtonTapHandler(_ handler: @escaping () -> Void) {
        self.doneButtonHandler = handler
    }
}
