//
//  CameraHeaderView.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 15.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit
import Foundation

class CameraHeaderView: UIView {

    var mainView: UIView!
    
    @IBOutlet weak var gradientImageView: UIImageView!
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var switchCamera: UIButton!
    @IBOutlet weak var toogleFlash: UIButton!
    
    
    @IBAction func closeButtonAction(_ sender: UIButton) {
        self.closeButtonHandler?()
    }
    
    @IBAction func toogleFlashAction(_ sender: UIButton) {
        toogleFlashButtonHandler?()
    }
    
    @IBAction func switchCameraAction(_ sender: UIButton) {
        switchCameraButtonHandler?()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        fromXib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        fromXib()
    }
    
    private func fromXib() {
        self.mainView = loadViewFromNib()
        mainView.frame = bounds
        mainView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        setupUI()
        addSubview(mainView)
    }
    
    private func loadViewFromNib() -> UIView {
        let bunlde = Bundle(for: type(of: self))
        let view = bunlde.loadNibNamed("CameraHeaderView", owner: self, options: nil)![0] as! UIView
        return view
    }
    
    private func setupUI() {
        self.closeButton.tintColor = Colors.white
        self.switchCamera.tintColor = Colors.white
        self.toogleFlash.tintColor = Colors.white
        
        self.closeButton.setImage(UIImage(named: "close_icon"), for: UIControlState())
        self.switchCamera.setImage(UIImage(named: "switch_camera_icon"), for: UIControlState())
        self.toogleFlash.setImage(UIImage(named: "flash_on"), for: UIControlState())
        self.gradientImageView.image = UIImage(named: "header_gradient")!
    }
    
    //MARK: Button Tap Handler
    
    fileprivate var closeButtonHandler: (() -> Void)?
    fileprivate var toogleFlashButtonHandler: (() -> Void)?
    fileprivate var switchCameraButtonHandler: (() -> Void)?
    
    func setCloseButtonTapHandler(_ handler: @escaping () -> Void) {
        self.closeButtonHandler = handler
    }
    
    func setFlashButtonTapHandler(_ handler: @escaping () -> Void) {
        self.toogleFlashButtonHandler = handler
    }
    
    func setSwitchCameraButtonTapHandler(_ handler: @escaping () -> Void) {
        self.switchCameraButtonHandler = handler
    }
}
