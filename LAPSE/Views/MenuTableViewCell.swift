//
//  MenuTableViewCell.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 10.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var menuItemLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
