//
//  SplashView.swift
//  PULSE
//
//  Created by Nikita Savchuk on 05.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit

class SplashView: UIView {

    private let screenCenterX = UIScreen.main.bounds.size.width / 2
    private let screenCenterY = UIScreen.main.bounds.size.height / 2
    private let screenLeadingX = UIScreen.main.bounds.minX
    private let screenLeadingY = UIScreen.main.bounds.minY
    private let screenTrailingX = UIScreen.main.bounds.maxX
    private let screenTrailingY = UIScreen.main.bounds.maxY
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        context?.setLineWidth(2.0)
        
//        let colorSpace = CGColorSpaceCreateDeviceRGB()
//        let components: [CGFloat] = [0.0, 0.0, 1.0, 1.0]
//        let color = CGColor(colorSpace: colorSpace, components: components)
        context?.setStrokeColor(UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1).cgColor)
        
        let dashArray: [CGFloat] = [4, 2]
        context?.setLineDash(phase: 2, lengths: dashArray)
        
        context?.move(to: CGPoint(x: screenLeadingX, y: screenCenterY))
        context?.addQuadCurve(to: CGPoint(x: screenTrailingX, y: screenCenterY + 10),
                              control: CGPoint(x: screenCenterX, y: screenCenterY))
//        context?.addLine(to: CGPoint(x: screenCenterX, y: screenCenterY + 15))
        context?.strokePath()
    }
}
