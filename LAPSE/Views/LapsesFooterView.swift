//
//  CameraFooterView.swift
//  LAPSE
//
//  Created by Nikita Savchuk on 14.02.17.
//  Copyright © 2017 Nikita Savchuk. All rights reserved.
//

import UIKit
import JMMarkSlider

class LapsesFooterView: UIView {

    @IBOutlet weak var slider: JMMarkSlider!
    
    @IBOutlet weak var gradientImageView: UIImageView!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    func setupStaticUI() {
        gradientImageView.image = UIImage(named: "footer_gradient")!
    }
    
    func setupSliderUI() {
        self.slider.markColor = Colors.white
        self.slider.markWidth = setupMarksWidth(for: 2)
        self.slider.markPositions = setupSliderPosition(forLapsesCount: 3)
        
        self.slider.handlerImage = UIImage(named: "slider_handler")
    }
    
    func setupMarksWidth(for numberOfLapses: Int) -> CGFloat {
        let minNumber = 1
        let maxNumber = 60
        
        let minWidth = 2
        let maxWidth = 335
        
        if numberOfLapses == minNumber {
            return CGFloat(maxWidth)
        } else if numberOfLapses == maxNumber {
            return CGFloat(minWidth)
        } else if numberOfLapses > maxNumber {
            NSLog("Number is too big")
            return 0
        } else if numberOfLapses < minNumber {
            NSLog("Number is too small")
            return 0
        } else {
            let width = maxWidth / numberOfLapses - ((numberOfLapses - 1) * 2)
            return CGFloat(width)
        }
    }
    
    func setupSliderPosition(forLapsesCount numberOfLapses: Int) -> [Any] {
        return [10,20,30]
    }
}
